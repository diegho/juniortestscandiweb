<!DOCTYPE html>
<html lang="en">
<script>
    function changeForm()
    {
        if (document.getElementById('productType').value == 'DVD')
        {
            document.getElementById('FurnitureDiv').style.display = 'none';
            document.getElementById('BookDiv').style.display = 'none';
            document.getElementById('DVDdiv').style.display = 'block';
        }
        else if (document.getElementById('productType').value == '0')
        {
            document.getElementById('FurnitureDiv').style.display = 'none';
            document.getElementById('BookDiv').style.display = 'none';
            document.getElementById('DVDdiv').style.display = 'none';
        }
        else if (document.getElementById('productType').value == 'Furniture')
        {
            document.getElementById('FurnitureDiv').style.display = 'block';
            document.getElementById('BookDiv').style.display = 'none';
            document.getElementById('DVDdiv').style.display = 'none';
        }
        else if(document.getElementById('productType').value == 'Book')
        {
            document.getElementById('FurnitureDiv').style.display = 'none';
            document.getElementById('BookDiv').style.display = 'block';
            document.getElementById('DVDdiv').style.display = 'none';
        }
    }
</script>
<script>
    function submitBTN()
    {
        document.getElementById('product_form').submit();
    }
</script>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Product List</title>

    <!-- Bootstrap CDN-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <style>
        .my-row
        {
            height: 600px;
            width: 100%;
        }

        .my-col
        {
            width: 20%;
        }

        .my-square
        {
            top: -25px;
            width: 100%;
            height: 450px;
        }
        .my-container-fluid
        {
            width: 100%;
        }

        .center
        {
            margin: auto;
            width: 50%;
        }

        .leftAllign
        {
            position: absolute;
            left: 0px;
            margin: auto;
            width: 60%;
        }

        .SWDallign
        {
            margin: auto;
            width: 30%;
        }

        .formsPadding
        {
            position: absolute;
            right: 180px;
        }

        .typesPadding
        {
            position: absolute;
            right: 230px;
        }

        .switcherUp
        {
            top: 145px;
        }

        .typeSwitcher
        {
            width: 10%;
            background: rgb(220,220,220);
            border: none;
            border-radius: 4px;
        }

        .null-character
        {
            content: '.';
            visibility: hidden;
        }

        .site-footer
        {
            background: rgb(220,220,220);
            position: absolute;
            bottom: 0;
            width: 100%;
        }

        .furnitureUP
        {
            top -125px;
        }

        #product_form
        {

        }

        #sku
        {
            
        }
        
        #name
        {
            
        }
        
        #price
        {
            
        }
        
        #productType
        {
            
        }
        
        #size
        {
            
        }
        
        #height
        {
            
        }
        
        #width
        {
            
        }
        
        #length
        {
            
        }
        
        #weight
        {
            
        }

    </style>

</head>
<body>
    <script type="text/javascript">
        function redirectBack()
        {
            window.location.replace("https://juniortestdieghocrestani.000webhostapp.com")
        }
    </script>
<!--start #header-->
<header id="header">
    <div class="strip d-flex justify-content-between px-4 py-1 bg-light">
        <p class="font-rale font-size-20 text-black-50 m-0">Prouct Add</p>
        <div class="font-rale font-size-14">
            <input type="submit" value="Save" forms="product_form" onclick="redirectBack(); submitBTN();">
            <input type="button" value="Cancel" onclick="redirectBack()">
        </div>
    </div>
</header>
<!--end #header-->
<header>
    <div class="null-character">
        .
    </div>
    <div class="null-character">
        .
    </div>
</header>
<!--start #main_page-->
<main id="main-site">
    <div class="leftAllign allButFooter">
        <div class="row my-row my-square leftAllign">
            <div class="col my-col">
                <form id="product_form" action="formsToDB.php" method="post">
                        SKU: <input class="formsPadding" type="text" name="s-k-u" id="sku">
                        <br>
                        <br>
                        Name: <input class="formsPadding" type="text" name="name" id="name">
                        <br>
                        <br>
                        Price ($): <input class="formsPadding" type="text" name="price" id="price">
                        <br>
                        <br>
                    Type Switcher:
                    <select id="productType" name="products" onclick="changeForm()">
                        <option value="0" selected="selected" id="closedOptions" >Choose...</option>
                        <option value="DVD">Size (MB)</option>
                        <option value="Furniture">Furniture</option>
                        <option value="Book">Book</option>
                    </select>
                    <br>
                    <div id="DVDdiv" style="display: none">
                        <br>
                        Size (MB): <input class="formsPadding" type="number" name="dvdMB" id="size">
                    </div>
                    <div id="FurnitureDiv" style="display: none">
                        <br>
                        Height (CM): <input class="formsPadding" type="text" name="HeightCM" id="height">
                        <br>
                        <br>
                        Width (CM): <input class="formsPadding" type="text" name="WidthCM" id="width">
                        <br>
                        <br>
                        Length (CM): <input class="formsPadding" type="text" name="LengthCM" id="length">
                        <br>
                        <br>
                    </div>
                        <br>
                    <div id="BookDiv" style="display: none">
                        Weight (KG): <input class="formsPadding" type="text" name="WeightKG" id="weight">
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
<!--!start #main_page-->

<!--start #footer-->
    <footer>
        <footer id="footer" class="site-footer py-5">
        </footer>
    </footer>
<!--!start #footer-->

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</body>
</html>
