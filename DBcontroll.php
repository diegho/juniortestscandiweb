<?php

class DBcontroll
{
    // Database connection properties
    protected $host='localhost';
    protected $user='<retracted user>';
    protected $password='<retracted pass>';
    protected $database='<retracted db name>';

    //connection property
    public $con = null;

    //call constructor
    public function __construct()
    {
        $this->con = mysqli_connect($this->host, $this->user, $this->password, $this->database);
        if($this->con->connect_error)
        {
            echo "Failed to connect" .$this->con->connect_error;
        }
    }

    public function __destruct()
    {
        $this->closeConnection();
    }

    //close MySQLi conection
    protected function closeConnection()
    {
        if($this->con != null)
        {
            $this->con->close();
            $this->con = null;
        }
    }

}