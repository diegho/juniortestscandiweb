<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Product List</title>

    <!-- Bootstrap CDN-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!--Custom CSS-->
    <?php
        //require functions.php
        //require ('functions.php');
    ?>

    <style>
        .my-row
        {
            height: 600px;
            width: 100%;
        }

        .my-col
        {
            width: 20%;
        }

        .my-square
        {
            margin: auto;
            width: 100%;
            height: 180px;
            border: 1px solid black;
        }
        .my-container-fluid
        {
            width: 100%;
        }

        .center
        {
            margin: auto;
            width: 50%;
        }

        .priceAllign
        {
            margin: auto;
            width: 20%;
        }

        .SWDallign
        {
            margin: auto;
            width: 30%;
        }

        .null-character
        {
            content: '.';
            visibility: hidden;
        }

        .site-footer
        {
            background: rgb(220,220,220);
        }

        #delete-product-btn
        {
            color: red;
        }

        .delete-checkbox
        {
            margin: auto;
            width: 5%;
            height: 5%;
        }

        #ADD
        {
            
        }

    </style>

</head>
<body>
<script type="text/javascript">
    function redirect()
    {
        window.location.replace("https://juniortestdieghocrestani.000webhostapp.com//addproduct.php");
    }
</script>
<!--start #header-->
<header id="header">
    <div class="strip d-flex justify-content-between px-4 py-1 bg-light">
        <p class="font-rale font-size-20 text-black-50 m-0">Prouct List</p>
        <div class="font-rale font-size-14">
            <div>
            <button id="ADD" onclick="redirect()">ADD</button>
            </div>
            <input type="submit" name="delete-btn" id="delete-product-btn" form="productForm" value="MASS DELETE"/>
        </div>
    </div>
</header>
<!--end #header-->
<header>
    <div class="null-character">
        .
    </div>
    <div class="null-character">
        .
    </div>
</header>
<!--start #main_page-->
<main id="main-site">

</main>
<!--!start #main_page-->
