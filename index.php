<?php
    //include header.php
    include('header.php');
?>
<?php

$servername = "localhost";
$username = '<retracted uuser name>';
$pass = '<retracted db pass>';
$db = '<retracted db name>';

$connect = mysqli_connect($servername, $username, $pass, $db);

if($connect->connect_error)
{
    die("Connection failed...". $connect->connect_error);
}

$query = mysqli_query($connect,"select * from form_table");
while ($row=mysqli_fetch_array($query))
{
?>
<div>
    <?php if ($row['size'] > 0)
    {

        echo '<style>
                .my-row
                {
                    height: 200px;
                    width: 100%;
                }
                .my-col
                {
                    width: 20%;
                }
                .my-square
                {
                    margin: auto;
                    width: 35%;
                    height: 180px;
                    border: 1px solid black;
                }
                .my-container-fluid
                {
                    overflow-wrap: break-word;
                    margin: auto;
                    max-width: 10000px;
                    display: grid;
                    grid-template-columns: repeat(auto-fit, minmax(300px,4fr));
                }    
                .center
                {
                    margin: auto;
                    width: 33%;
                    padding-left: 4px;
                }
                
                .allignTotal
                {
                    margin: auto;
                    width: 50%;
                }
                
                .weightAllign
                {
                    margin: auto;
                    width: 35%;
                }
                
                .delete-checkbox
                {
                    width: 50%;
                    height: 20px;
                    position: relative;
                    right: 87px;
                    top: 12px;
                }
                #wordWrap
                {
                    word-wrap: break-word;
                }
                
                .fontSize
                {
                    font-size: 20px;
                }
                
             </style>
             <div class="my-container-fluid">
                    <div class="row my-row">
                        <div class="col my-col">
                            <div class="col my-square">
                                <div class="custom-checkbox">
                                <form action="deleteBTN.php" id="productForm" method="POST">
                                <input type="checkbox" name="delete_id[]" value='.$row["submited_id"].' class="delete-checkbox">
                                </div>
                                <div class="center fontSize">
                                    '.$row["sku"]. "<br>".'
                                </div>
                                <div class="center fontSize">
                                    '.$row["name"]. "<br>".'
                                </div>
                                <div class="priceAllign fontSize">
                                    '.$row["price"]. "$<br>".'
                                </div>
                                <div class="center fontSize">
                                    Size: '.$row["size"]. "MB".'
                                </div>
                            </div>
                        </div>
                    </div>
                </div>';
    }
    else if ($row['height'] > 0)
    {
        echo '<style>
                .my-row
                {
                    height: 200px;
                    width: 100%;
                }
                .my-col
                {
                    width: 20%;
                }
                .my-square
                {
                    margin: auto;
                    width: 35%;
                    height: 180px;
                    border: 1px solid black;
                }
                .my-container-fluid
                {
                    overflow-wrap: break-word;
                    margin: auto;
                    max-width: 10000px;
                    display: grid;
                    grid-template-columns: repeat(auto-fit, minmax(300px,4fr));
                }    
                .center
                {
                    margin: auto;
                    width: 33%;
                    padding-left: 4px;
                }
                
                .allignTotal
                {
                    margin: auto;
                    width: 50%;
                }
                
                .weightAllign
                {
                    margin: auto;
                    width: 35%;
                }
                
                .delete-checkbox
                {
                    width: 50%;
                    height: 20px;
                    position: relative;
                    right: 87px;
                    top: 12px;
                }
                #wordWrap
                {
                    word-wrap: break-word;
                }
                
                .fontSize
                {
                    font-size: 20px;
                }
                
                #dontOverflow
                {
                    white-space: nowrap;
                    overflow: hidden;
                    text-overflow: ellipsis;
                }   
                
             </style>
        <div class="my-container-fluid" id="dontOverflow">
                    <div class="row my-row">
                        <div class="col my-col">
                            <div class="col my-square">
                                <div class="custom-checkbox">
                                <form action="deleteBTN.php" id="productForm" method="POST">
                                <input type="checkbox" name="delete_id[]" value='.$row["submited_id"].' class="delete-checkbox">
                                </div>
                                <div class="center fontSize">
                                    '.$row["sku"]. "<br>".'
                                </div>
                                <div class="center fontSize">
                                    '.$row["name"]. "<br>".'
                                </div>
                                <div class="priceAllign fontSize">
                                    '.$row["price"]. "$<br>".'
                                </div>
                                <div class="allignTotal fontSize">
                                    Dimension: '.$row["height"]. "x".''.$row['width']. "x".''.$row['length'].'
                                </div>
                            </div>
                        </div>
                    </div>
                </div>';
    }
    else if ($row['weight'] > 0)
    {
        echo '<style>
                .my-row
                {
                    height: 200px;
                    width: 100%;
                }
                .my-col
                {
                    width: 20%;
                }
                .my-square
                {
                    margin: auto;
                    width: 35%;
                    height: 180px;
                    border: 1px solid black;
                }
                .my-container-fluid
                {
                    overflow-wrap: break-word;
                    margin: auto;
                    max-width: 10000px;
                    display: grid;
                    grid-template-columns: repeat(auto-fit, minmax(300px,4fr));
                }    
                .center
                {
                    margin: auto;
                    width: 33%;
                    padding-left: 4px;
                }
                
                .allignTotal
                {
                    margin: auto;
                    width: 50%;
                }
                
                .weightAllign
                {
                    margin: auto;
                    width: 35%;
                }
                
                .delete-checkbox
                {
                    width: 50%;
                    height: 20px;
                    position: relative;
                    right: 87px;
                    top: 12px;
                }
                #wordWrap
                {
                    word-wrap: break-word;
                }
                
                .fontSize
                {
                    font-size: 20px;
                }
                
             </style>
        <div class="my-container-fluid">
                    <div class="row my-row">
                        <div class="col my-col">
                            <div class="col my-square">
                                <div class="custom-checkbox">
                                <form action="deleteBTN.php" id="productForm" method="POST">
                                <input type="checkbox" name="delete_id[]" value='.$row["submited_id"].' class="delete-checkbox">
                                </div>
                                <div class="center fontSize">
                                    '.$row["sku"]. "<br>".'
                                </div>
                                <div class="center fontSize">
                                    '.$row["name"]. "<br>".'
                                </div>
                                <div class="priceAllign fontSize">
                                    '.$row["price"]. "$<br>".'
                                </div>
                                <div class="weightAllign fontSize">
                                    Weight: '.$row["weight"]. "KG".'
                                </div>
                            </div>
                        </div>
                    </div>
                </div>';
    }
    ?>

</div>
<?php } ?>

    
<?php
//include footer.php
include('footer.php')
?>